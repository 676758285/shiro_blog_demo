package com.dong.shiro;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class UserLoginController {

    @Autowired
    MyShiro myShiro;

    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public Map<String,Object> userLoginAction (@RequestParam(value = "userName") String userName,
                                               @RequestParam(value = "password") String password){

        Map<String,Object> resultMap = myShiro.userLoginAction(userName,password);

        return resultMap;

    }

    //自定义Realm的认真过程.
    @RequestMapping(value = "/realmLogin",method = RequestMethod.POST)
    public Map<String,Object> userLoginWithRealmAction (@RequestParam(value = "userName") String userName,
                                               @RequestParam(value = "password") String password){

        Map<String,Object> resultMap = myShiro.userLoginActionWithMyRealm(userName,password);

        return resultMap;

    }
}
