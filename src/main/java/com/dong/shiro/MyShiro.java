package com.dong.shiro;

import org.apache.commons.collections.ArrayStack;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;
import org.springframework.stereotype.Component;

import java.sql.Array;
import java.util.*;


@Component
public class MyShiro {


    public Map<String,Object> userLoginAction (String userName,String passWord){

        Map<String,Object> resultMap = new HashMap<>();

        //初始化SecurityManager对象 使用INI文件编写用户认证授权信息
        Factory<SecurityManager> factory = new IniSecurityManagerFactory("classpath:shiro.ini");
        //通过SecurityManager工厂对象,获取SecurityManager实例对象.
        SecurityManager securityManager =  factory.getInstance();

        // 把 securityManager 实例 绑定到 SecurityUtils
        SecurityUtils.setSecurityManager(securityManager);

        //组建Subject主体.
        Subject subject = SecurityUtils.getSubject();

        //创建 token 令牌
        UsernamePasswordToken token = new UsernamePasswordToken(userName,passWord);

        //用户登录操作.
        try{
            subject.login(token);
            resultMap.put("code","200");
            resultMap.put("msg","用户登录成功");

            if (subject.hasRole("role1")){
                resultMap.put("roleMsg1","用户拥有角色1");
            }else {
                resultMap.put("roleMsg1","用户未拥有角色1");

            }

            if (subject.hasAllRoles(Arrays.asList("role1","role2"))){
                resultMap.put("roleMsg2","用户同时拥有角色1和角色2");
            }else {
                resultMap.put("roleMsg2","用户未同时拥有角色1和角色2");
            }

            //这里就不返回第三种方法了,直接打印了
            System.out.println(Arrays.asList(subject.hasRoles(Arrays.asList("role1","role2"))));

            if (subject.isPermitted("permission1")){
                resultMap.put("PermittedMsg1","用户拥有权限1");
            }else {
                resultMap.put("PermittedMsg1","用户未拥有权限1");

            }

            if (subject.isPermittedAll("permission2","permission3")){
                resultMap.put("PermittedMsg2","用户同时拥有权限1和权限2");
            }else {
                resultMap.put("PermittedMsg2","用户未同时拥有权限1和权限2");
            }


        }catch (AuthenticationException e){
            //登录失败原因 1 用户不存在 2 用户密码不正确
            resultMap.put("code","-1");
            resultMap.put("msg","用户登录失败");
        }
        return resultMap;

    }

    public Map<String,Object> userLoginActionWithMyRealm (String userName,String passWord){

        Map<String,Object> resultMap = new HashMap<>();

        //初始化SecurityManager对象 使用INI文件进行自定义Realm的设置
        Factory<SecurityManager> factory = new IniSecurityManagerFactory("classpath:shiro-realm.ini");

        //通过SecurityManager工厂对象,获取SecurityManager实例对象.
        SecurityManager securityManager =  factory.getInstance();

        // 把 securityManager 实例 绑定到 SecurityUtils
        SecurityUtils.setSecurityManager(securityManager);

        //组建Subject主体.
        Subject subject = SecurityUtils.getSubject();

        //创建 token 令牌
        UsernamePasswordToken token = new UsernamePasswordToken(userName,passWord);

        //用户登录操作.
        try{
            subject.login(token);
            resultMap.put("code","200");
            resultMap.put("msg","用户登录成功");

            if (subject.isPermitted("add")){
                resultMap.put("PermittedMsg","用户拥有add权限");
            }else {
                resultMap.put("PermittedMsg","用户未拥有add权限");
            }
            if (subject.hasRole("superAdmin")){
                resultMap.put("roleMsg","用户拥有superAdmin角色");
            }else {
                resultMap.put("roleMsg","用户未拥有superAdmin角色");
            }

        }catch (AuthenticationException e){
            //登录失败原因 1 用户不存在 2 用户密码不正确
            resultMap.put("code","-1");
            resultMap.put("msg","用户登录失败");
        }
        return resultMap;

    }



}
