package com.dong.shiro;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HtmlController {

    @RequestMapping(value = "/index")
    public String userLoginHtmlAction (){

        return "login";
    }

    @RequestMapping(value = "/home")
    public String userHomeHtmlAction (){

        return "home";
    }

}




